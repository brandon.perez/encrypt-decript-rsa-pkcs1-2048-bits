import React, { useState } from 'react'
import { ToastContainer, toast } from 'react-toastify'
import {
  Box,
  Container,
  Grid,
  Typography,
  TextField,
  Button,
  Card,
  CardContent,
  Link,
} from '@material-ui/core'

import { localEncrypt, localDecrypt } from '@utils/edrsa.util'

export default function IndexPage(): JSX.Element {
  const [privateKey, setPrivateKey] = useState<string>('')
  const [publicKey, setPublicKey] = useState<string>('')
  const [encryptedString, setEncryptedString] = useState<string>('')
  const [desencryptedString, setDesencryptedString] = useState<string>('')

  const onEncrypt = async () => {
    try {
      const sendEncrypted = localEncrypt(publicKey, desencryptedString)
      setEncryptedString(sendEncrypted)
      toast.success('Encriptación realizada correactamente.')
    } catch (error) {
      console.log(error)
      toast.error('Hubo un error en la encriptación.')
    }
  }

  const onDesencrypt = async () => {
    try {
      const sendDesencrypted = localDecrypt(privateKey, encryptedString)
      setDesencryptedString(sendDesencrypted)
      toast.success('Desencriptación realizada correactamente.')
    } catch (error) {
      console.log(error)
      toast.error('Hubo un error en la desencriptación.')
    }
  }

  return (
    <React.Fragment>
      <Box marginY={6}>
        <Container maxWidth="md">
          <Grid container direction="row" spacing={2}>
            <Grid item xs={12}>
              <Grid container justify="center">
                <Grid item xs={12}>
                  <Typography variant="h3" align="center">
                    Encrypt/Decrypt RSA PKCS1 2048-bit
                  </Typography>
                </Grid>
                <Grid item xs={8}>
                  <Typography variant="body1" align="center">
                    Antes de encriptar o desencriptar texto se deben de ingresar
                    las private/public keys. Se pueden descargar las llaves
                    desde los siguientes enlaces:
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant="subtitle1" align="center">
                    <Link href="/privateKey.pem">Private Key</Link>
                    {' - '}
                    <Link href="/publicKey.pem">Public Key</Link>
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={6}>
              <Card>
                <CardContent>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <Typography variant="h5">Private Key</Typography>
                      <TextField
                        multiline
                        fullWidth
                        id="encrypt"
                        name="encrypt"
                        variant="outlined"
                        placeholder="Private Key"
                        rows={12}
                        value={privateKey}
                        onChange={(e) => setPrivateKey(e.target.value)}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant="h5">Public Key</Typography>
                      <TextField
                        multiline
                        fullWidth
                        id="encrypt"
                        name="encrypt"
                        variant="outlined"
                        placeholder="Public Key"
                        rows={12}
                        value={publicKey}
                        onChange={(e) => setPublicKey(e.target.value)}
                      />
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </Grid>
            <Grid item xs={6}>
              <Card>
                <CardContent>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <Typography variant="h5">Encrypt</Typography>
                      <TextField
                        multiline
                        fullWidth
                        id="encrypt"
                        name="encrypt"
                        variant="outlined"
                        placeholder="Encrypt"
                        rows={12}
                        value={desencryptedString}
                        onChange={(e) => setDesencryptedString(e.target.value)}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant="h5">Decrypt</Typography>
                      <TextField
                        multiline
                        fullWidth
                        id="encrypt"
                        name="encrypt"
                        variant="outlined"
                        placeholder="Decrypt"
                        rows={12}
                        value={encryptedString}
                        onChange={(e) => setEncryptedString(e.target.value)}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Grid container spacing={2}>
                        <Grid item xs={6}>
                          <Button
                            variant="contained"
                            fullWidth
                            color="primary"
                            onClick={onEncrypt}
                          >
                            Encrypt
                          </Button>
                        </Grid>
                        <Grid item xs={6}>
                          <Button
                            variant="contained"
                            fullWidth
                            color="secondary"
                            onClick={onDesencrypt}
                          >
                            Decrypt
                          </Button>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </Container>
      </Box>
      <ToastContainer />
    </React.Fragment>
  )
}
