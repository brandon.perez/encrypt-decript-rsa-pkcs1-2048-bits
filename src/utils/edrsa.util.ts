import { constants } from 'crypto'
import NodeRSA from 'node-rsa'

export const localEncrypt = (publicKey: string, string: string): string => {
  console.log({
    publicKey,
    string,
  })
  const publicKeyRSA = new NodeRSA(null, 'pkcs1', {
    encryptionScheme: {
      scheme: 'pkcs1',
      padding: constants.RSA_PKCS1_PADDING,
    },
  })
  publicKeyRSA.importKey(publicKey, 'pkcs1-public-pem')
  return publicKeyRSA.encrypt(string, 'base64')
}

export const localDecrypt = (privateKey: string, string: string): string => {
  console.log({
    privateKey,
    string,
  })
  const privateKeyRSA = new NodeRSA(null, 'pkcs1', {
    encryptionScheme: {
      scheme: 'pkcs1',
      padding: constants.RSA_PKCS1_PADDING,
    },
  })
  privateKeyRSA.importKey(privateKey, 'pkcs1-private-pem')
  return privateKeyRSA.decrypt(string, 'utf8')
}
