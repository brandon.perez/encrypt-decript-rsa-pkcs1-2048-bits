# LuegopaGo SOAT Web

Proyecto de Landing Page Web para LuegopaGo SOAT realizado en Next.js y Typescript.

## Despliegue ambientes de prueba

Despliega este proyecto usando [Vercel](https://vercel.com?utm_source=github&utm_medium=readme&utm_campaign=next-example):

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/git/external?repository-url=https://github.com/vercel/next.js/tree/canary/examples/with-typescript&project-name=with-typescript&repository-name=with-typescript)

## ¿Cómo usarlo?
#### Instalación
```bash
$ cd luegopago-soat-front-web
$ yarn install
```
#### Development
```bash
$ yarn dev
```
#### Production
Genera archivos estáticos de las páginas ubicadas en `./src/pages/`
```bash
$ yarn build
$ yarn start
```
Despliegue con [Vercel](https://vercel.com/new?utm_source=github&utm_medium=readme&utm_campaign=next-example) ([Documentación](https://nextjs.org/docs/deployment)).